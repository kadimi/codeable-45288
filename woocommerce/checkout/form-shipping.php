<?php
/**
 * Checkout shipping information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-shipping.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>





<div class="woocommerce-shipping-fields" style="padding-left:10px;"> <!-- START -->
    
    
				<div class="entry-content">
			<div class="vc_row wpb_row vc_row-fluid"><div class="col-a wpb_column vc_column_container vc_col-sm-8"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
		
           
    
	<?php if ( true === WC()->cart->needs_shipping_address() ) : ?>

		<h3 id="ship-to-different-address" style="display:none;">
			<label for="ship-to-different-address-checkbox" class="checkbox"><?php _e( 'Ship to a different address?', 'woocommerce' ); ?></label>
			<input id="ship-to-different-address-checkbox" class="input-checkbox" <?php checked( apply_filters( 'woocommerce_ship_to_different_address_checked', 'shipping' === get_option( 'woocommerce_ship_to_destination' ) ? 1 : 0 ), 1 ); ?> type="checkbox" name="ship_to_different_address" value="1" />
		</h3>

		<div class="shipping_address">

			<?php do_action( 'woocommerce_before_checkout_shipping_form', $checkout ); ?>

			<?php foreach ( $checkout->checkout_fields['shipping'] as $key => $field ) : ?>

				<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>

			<?php endforeach; ?>

			<?php do_action( 'woocommerce_after_checkout_shipping_form', $checkout ); ?>

		</div>

	<?php endif; ?>

	<?php do_action( 'woocommerce_before_order_notes', $checkout ); ?>

	<?php if ( apply_filters( 'woocommerce_enable_order_notes_field', get_option( 'woocommerce_enable_order_comments', 'yes' ) === 'yes' ) ) : ?>

		<?php if ( ! WC()->cart->needs_shipping() || wc_ship_to_billing_address_only() ) : ?>

			<h3><?php _e( 'Additional Information', 'woocommerce' ); ?></h3>

		<?php endif; ?>

		<?php foreach ( $checkout->checkout_fields['order'] as $key => $field ) : ?>

			<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>

		<?php endforeach; ?>

	<?php endif; ?>

	<?php do_action( 'woocommerce_after_order_notes', $checkout ); ?>
  
            
            
<!--            
<div class="need-help-box"  style=" border-top: 1px solid #ccc;  margin-top: -25px;">
<h4>Need Help?</h4>
<p>Contact our customer service team on<br/>
<strong style="font-size: 24px;">0800 411 <span>88 99</span></strong><br/>
(Calls may be recorded and monitored for quality and training purposes.)</p>

</div>     
    -->
            
<!-- HEED HELP? -->
<div class="entry-content" style=" margin-top: -25px;">
<div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12 vc_hidden-xs"><div class="vc_column-inner "><div class="wpb_wrapper">
<div class="wpb_raw_code wpb_content_element wpb_raw_html">
<div class="wpb_wrapper">
<div class="need-help-box">
<hr>
<h4>Need Help?</h4>
<p>Contact our customer service team on<br>
<span style="font-size: 24px;">0800 411 88 99</span><br>
(Calls may be recorded and monitored for quality and training purposes.)</p>
</div>
</div>
</div></div></div></div></div>


<div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12 vc_hidden-lg vc_hidden-md vc_hidden-sm"><div class="vc_column-inner "><div class="wpb_wrapper">
<div class="wpb_raw_code wpb_content_element wpb_raw_html">
<div class="wpb_wrapper">
<div class="need-help-box">
<hr>
<h4>Need Help?</h4>
<p>Contact our customer service team on<br>
<a class="tel" href="tel:0800 411 88 99" style="color:#43454b; text-decoration:none;">
<span style="font-size: 24px; color: #ff0004;">0800 411 88 99</span></a>
<!--<span class="telTM" style="font-size: 24px; color: #ff0004;">0800 411 88 99</span>-->  <br>
(Calls may be recorded and monitored for quality and training purposes.)</p>
</div>
</div>
</div></div></div></div></div>
</div>
<!-- END OF HEED HELP? -->
            
       
<!-- Order Summary -->
            <div class="OrderSummarybox"> 
    <div class="MobileOnly">           
<div class="borderboxMobile" style="margin-top:20px; margin-bottom:20px;">           
<div class="redwidget-brand" style="margin-top:-20px;"> <h2 style="text-align: center; color: #ffffff !important;">Order Summary</h2></div>
<br/>     
<center><h3>Credit/Debit card</h3></center>
<center><span class="adtotalamount" ><?php echo do_shortcode('[cart_total]');?></span></center>
<p style="text-align: center;">One off payment</p>   
<br/>   
<p style="text-align: center; margin-top:-10px; margin-bottom:10px;">
<img class="aligncenter size-full wp-image-3286" src="http://www.applianceserve.co.uk/getaquote/wp-content/uploads/2017/01/payment-cards.png" alt="payment-cards"/>
</p>        

</div>
<!--
 <div class="borderboxtandc"> 
<p style="text-align: center; padding:15px;">
<a href="http://www.applianceserve.co.uk/termsandconditions/" target="_blank">Terms &amp; Conditions</a></p>
</div>-->
<!-- End of Order Summary -->

<div class="jcmc-buttons" style="display: inline!important;">
<a href="#" class="jcmc-button jcmc-nextprev jcmc-next">Next</a> 
<a href="#" class="jcmc-button jcmc-nextprev jcmc-prev" style="display: inline-block; background: #CCCCCC!important; color: #666666;">Previous</a>    
</div>     
   </div>             
    
  </div>  <!-- End of Order Summary -->         
              
            
		</div>
	</div>
</div></div></div><div class="col-b wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
                        <!-- CONTENT
                <div class="h_iframe-s">
         <img class="ratio" src="http://www.news-junction.com/images/space.png">
        <iframe src="http://www.news-junction.com/WebQuoteApp/widget-personaldetails/" frameborder="0" allowfullscreen></iframe>
    </div> -->
  <!-- END CONTENT -->
			</div>
	</div>
</div></div></div></div>
</div><!-- .entry-content -->
    
    
    
    
    
    
    
    
    
    
</div> <!-- END -->

        
