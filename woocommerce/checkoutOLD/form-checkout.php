<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->enable_signup && ! $checkout->enable_guest_checkout && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
	return;
}

?>

<style>
.jcmc-buttons {display:none;}
    #jcmc-wrap .jcmc-nextprev, #jcmc-wrap .jcmc-nextprev:visited {
    background: #339933 none repeat scroll 0 0;
    color: #FFFFFF;
    width: 100%;margin-top: 15px;
}

.jcmc-prev {
    background: #CCCCCC none repeat scroll 0 0;
    color: #666666;
    width: 100%;margin-top: 15px;
}        .borderbox {
    border: 1px solid #cccccc;
    margin-top: 20px;
}

</style>

<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

	<?php if ( sizeof( $checkout->checkout_fields ) > 0 ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div class="col2-set" id="customer_details">
			<div class="col-1">
				<?php do_action( 'woocommerce_checkout_billing' ); ?>
			</div>

			<div class="col-2">
				<?php do_action( 'woocommerce_checkout_shipping' ); ?>
			</div>
		</div>

		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

	<?php endif; ?>

	<div id="order_review_heading"> <!-- -->
        
	<?php aps_steps( 1 ); ?> 
        
        
<div class="vc_row wpb_row vc_row-fluid row-appointment"><div class="wpb_column vc_column_container vc_col-sm-8"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="vc_row wpb_row vc_inner vc_row-fluid content-appointment"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
         <h3>Appointment Date</h3>
        <p>Please select your preferred appointment date. We don’t currently conduct repairs at the weekends</p>
        <hr>
            
            	<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>
         
            
            <!--
    <div class="need-help-box"  style="margin-top:-35px;">
<hr />
<h4>Need Help?</h4>
<p>Contact our customer service team on<br/>
<strong style="font-size: 24px;">0800 411 <span>88 99</span></strong><br/>
(Calls may be recorded and monitored for quality and training purposes.)</p>

</div>-->
          
                     
<!-- HEED HELP? -->
<div class="entry-content" style="margin-top: -35px;">
<div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12 vc_hidden-xs"><div class="vc_column-inner "><div class="wpb_wrapper">
<div class="wpb_raw_code wpb_content_element wpb_raw_html">
<div class="wpb_wrapper">
<div class="need-help-box">
<hr>
<h4>Need Help?</h4>
<p>Contact our customer service team on<br>
<span style="font-size: 24px;">0800 411 88 99</span><br>
(Calls may be recorded and monitored for quality and training purposes.)</p>
</div>
</div>
</div></div></div></div></div>


<div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12 vc_hidden-lg vc_hidden-md vc_hidden-sm"><div class="vc_column-inner "><div class="wpb_wrapper">
<div class="wpb_raw_code wpb_content_element wpb_raw_html">
<div class="wpb_wrapper">
<div class="need-help-box">
<hr>
<h4>Need Help?</h4>
<p>Contact our customer service team on<br>
<a class="tel" tabindex="-1" href="tel:0800 411 88 99" style="color:#43454b;">
<span style="font-size: 24px;">0800 411 88 99</span></a><br>
(Calls may be recorded and monitored for quality and training purposes.)</p>
</div>
</div>
</div></div></div></div></div>
</div>
<!-- END OF HEED HELP? -->

		</div>
	</div>
</div></div></div></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_raw_code wpb_content_element wpb_raw_html">
		<div class="wpb_wrapper">
            
                       <!-- Order Summary -->
            
<div class="borderbox">           
<div class="redwidget-brand" style="margin-top:-20px;">  <h2 style="text-align: center; color: #ffffff !important;">Order Summary</h2></div>
<br/>     
<center><h3>Credit/Debit card</h3></center>
<center><span class="adtotalamount" ><?php echo do_shortcode('[cart_total]');?></span></center>
<p style="text-align: center;">One off payment</p>   
<br/>   
<p style="text-align: center; margin-top:-10px; margin-bottom:10px;">
<img class="aligncenter size-full wp-image-3286" src="http://www.apswebquote.com/images/payment-cards.png" alt="payment-cards"/>
</p>        
</div>
<!-- End of Order Summary -->

            
<div class="jcmc-buttons" style="display: inline!important;">
    <a href="#" class="jcmc-button jcmc-nextprev jcmc-next">Next</a>
    <!-- <a href="#" class="jcmc-button jcmc-nextprev jcmc-prev" style="display: inline-block; background: #CCCCCC!important;
    color: #666666;">Previous</a> -->
</div>
            
            
		<!--   <div class="h_iframe">
            <img class="ratio" src="http://www.news-junction.com/images/space.png">
        <iframe src="http://www.news-junction.com/WebQuoteApp/widget-appointment/" frameborder="0" allowfullscreen></iframe>
		</div>-->
            
	</div>
</div></div></div></div>
    
    
    
    
    </div><!-- -->

    
<style>
.jckwds-fields h3 {display:none;}
.jckwds-fields label {display:none;}
</style>
    
    
    
<!-- woocommerce_checkout_before_order_review php was here -->

       
    
    
    
	<div id="order_review" class="woocommerce-checkout-review-order">
		<?php do_action( 'woocommerce_checkout_order_review' ); ?>
	</div>

	<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>

</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
