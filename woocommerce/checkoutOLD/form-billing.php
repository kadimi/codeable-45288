<?php
/**
 * Checkout billing information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-billing.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.1.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/** @global WC_Checkout $checkout */

?>

<style>
.jcmc-buttons {display:none;}
    #jcmc-wrap .jcmc-nextprev, #jcmc-wrap .jcmc-nextprev:visited {
    background: #339933 none repeat scroll 0 0 !important;
    color: #FFFFFF;
    width: 100%;
        margin-top: 15px;
}

.jcmc-prev {
    background: #CCCCCC none repeat scroll 0 0 !important;
    color: #666666;
    width: 100%;margin-top: 15px;
}
        .borderbox {
    border: 1px solid #cccccc;
    margin-top: 20px;
}
</style>

<div class="woocommerce-billing-fields"> <!-- START -->

<link rel='stylesheet' id='woomultisteps'  href='http://news-junction.com/css/woomultisteps.css' type='text/css' />

<!-- MultiSteps 1 - Personal Details -->


    <div id="aps-steps" class=" aps-steps-tabs-top">
<ul class="aps-steps-tabs  aps-steps-arrows aps-steps-blocks aps-steps-wide" id="aps-steps-tabs">
<li class="aps-steps-enabled aps-steps-odd " style="color: white;"><a id="aps-steps-trigger-0" href="#"><span class="aps-steps-tab-span"><span class="aps-steps-number">1.</span>Appointment Date</span></a></li>
<li class="aps-steps-odd  aps-steps-active-link"><a id="aps-steps-trigger-1" href="#"><span class="aps-steps-tab-span"><span class="aps-steps-number">2.</span>Personal Details</span></a></li>
<li class="aps-steps-even"><a id="aps-steps-trigger-2" href="#"><span class="aps-steps-tab-span"><span class="aps-steps-number">3.</span>Payment Details</span></a></li>
<li class="aps-steps-odd"><a id="aps-steps-trigger-3" href="#"><span class="aps-steps-tab-span"><span class="aps-steps-number">4.</span>Confirmation</span></a></li>
</ul>
</div>
    

<div class="entry-content">
<div class="vc_row wpb_row vc_row-fluid"><div class="col-a wpb_column vc_column_container vc_col-sm-8"><div class="vc_column-inner "><div class="wpb_wrapper">
<div class="wpb_text_column wpb_content_element ">
<div class="wpb_wrapper">

    
    
    <?php if ( wc_ship_to_billing_address_only() && WC()->cart->needs_shipping() ) : ?>

		<h3><?php _e( 'Billing &amp; Shipping', 'woocommerce' ); ?></h3>

	<?php else : ?>
    
     <!-- CONTENT -->

    
            <h3>Personal Details</h3>
<p>Please enter your details so we are able to contact you.</p>
    
    <hr>
    
    
		<!--<h3><?php _e( 'Billing Details', 'woocommerce' ); ?></h3>-->

	<?php endif; ?>

	<?php do_action( 'woocommerce_before_checkout_billing_form', $checkout ); ?>

	<?php foreach ( $checkout->checkout_fields['billing'] as $key => $field ) : ?>

		<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>

	<?php endforeach; ?>

    <h3>Your Address</h3> 
	<?php do_action('woocommerce_after_checkout_billing_form', $checkout ); ?>

	<?php if ( ! is_user_logged_in() && $checkout->enable_signup ) : ?>

		<?php if ( $checkout->enable_guest_checkout ) : ?>

			<p class="form-row form-row-wide create-account">
				<input class="input-checkbox" id="createaccount" <?php checked( ( true === $checkout->get_value( 'createaccount' ) || ( true === apply_filters( 'woocommerce_create_account_default_checked', false ) ) ), true) ?> type="checkbox" name="createaccount" value="1" /> <label for="createaccount" class="checkbox"><?php _e( 'Create an account?', 'woocommerce' ); ?></label>
			</p>

		<?php endif; ?>

		<?php do_action( 'woocommerce_before_checkout_registration_form', $checkout ); ?>

		<?php if ( ! empty( $checkout->checkout_fields['account'] ) ) : ?>

			<div class="create-account">

				<p><?php _e( 'Create an account by entering the information below. If you are a returning customer please login at the top of the page.', 'woocommerce' ); ?></p>

				<?php foreach ( $checkout->checkout_fields['account'] as $key => $field ) : ?>

					<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>

				<?php endforeach; ?>

				<div class="clear"></div>

			</div>

		<?php endif; ?>

		<?php do_action( 'woocommerce_after_checkout_registration_form', $checkout ); ?>

	<?php endif; ?>
    

     <!-- END CONTENT -->

</div>
	</div>
    
</div></div></div><div class="col-b wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
        

            <!-- CONTENT-->
            
            <!-- Order Summary -->
 <div class="noMobile">            
<div class="borderbox">           
<div class="redwidget-brand" style="margin-top:-20px;"> <h2 style="text-align: center; color: #ffffff !important;">Order Summary</h2></div>
<br/>     
<center><h3>Credit/Debit card</h3></center>
<center><span class="adtotalamount" ><?php echo do_shortcode('[cart_total]');?></span></center>
<p style="text-align: center;">One off payment</p>   
<br/>   
<p style="text-align: center; margin-top:-10px; margin-bottom:10px;">
<img class="aligncenter size-full wp-image-3286" src="http://www.apswebquote.com/images/payment-cards.png" alt="payment-cards"/>
</p>        
</div>
 
<!-- End of Order Summary -->

<div class="jcmc-buttons" style="display: inline!important;">
<a href="#" class="jcmc-button jcmc-nextprev jcmc-next">Next</a> 
<a href="#" class="jcmc-button jcmc-nextprev jcmc-prev" style="display: inline-block; background: #CCCCCC!important;
    color: #666666;">Previous</a>    
</div>
 </div>            
            <!-- iFrame 
                <div class="h_iframe">
            <img class="ratio" src="http://www.news-junction.com/images/space.png">
        <iframe src="http://www.news-junction.com/WebQuoteApp/widget-personaldetailsv1/" frameborder="0" allowfullscreen></iframe>
                  
    </div> -->
    
  <!-- END CONTENT -->
		</div>
	</div>
</div></div></div></div>
</div><!-- .entry-content -->




    
    
    
</div> <!-- END -->
            
           
