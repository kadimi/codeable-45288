<?php
/**
 * Checkout Payment Section
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/payment.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.5.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! is_ajax() ) {
	do_action( 'woocommerce_review_order_before_payment' );
}
?>
<div id="payment" class="woocommerce-checkout-payment"> <!-- START -->
    
    <style>
.jcmc-buttons {display:none;}
        #jcmc-wrap .jcmc-nextprev, #jcmc-wrap .jcmc-nextprev:visited {
    background: #339933 none repeat scroll 0 0;
    color: #FFFFFF;
    width: 100%;margin-top: 15px;
}

.jcmc-prev {
    background: #CCCCCC none repeat scroll 0 0;
    color: #666666;
    width: 100%;margin-top: 15px;
        }
        .borderbox {
    border: 1px solid #cccccc;
    margin-top: 20px;
}

</style>
    
<!-- MultiSteps 2 - Payment -->
<div id="aps-steps" class=" aps-steps-tabs-top">
<ul class="aps-steps-tabs  aps-steps-arrows aps-steps-blocks aps-steps-wide" id="aps-steps-tabs">
<li class="aps-steps-enabled aps-steps-odd"><a id="aps-steps-trigger-0" href="#"><span class="aps-steps-tab-span"><span class="aps-steps-number">1.</span>Appointment Date</span></a></li>    
<li class="aps-steps-enabled aps-steps-odd"><a id="aps-steps-trigger-1" href="#"><span class="aps-steps-tab-span"><span class="aps-steps-number">2.</span>Personal Details</span></a></li>
<li class="aps-steps-even aps-steps-enabled aps-steps-active-link"><a id="aps-steps-trigger-2" href="#"><span class="aps-steps-tab-span"><span class="aps-steps-number">3.</span>Payment Details</span></a></li>
<li class="aps-steps-odd"><a id="aps-steps-trigger-3" href="#"><span class="aps-steps-tab-span"><span class="aps-steps-number">4.</span>Confirmation</span></a></li>
</ul>
</div> 


				<div class="entry-content">
			<div class="vc_row wpb_row vc_row-fluid"><div class="col-a wpb_column vc_column_container vc_col-sm-8"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">

<h3>Payment</h3>
<p>Please enter your payment details.</p>
      <hr>
            
          <h3> Credit / Debit Card Payment  </h3>
    
	<?php if ( WC()->cart->needs_payment() ) : ?>
		<ul class="wc_payment_methods payment_methods methods">
			<?php
				if ( ! empty( $available_gateways ) ) {
					foreach ( $available_gateways as $gateway ) {
						wc_get_template( 'checkout/payment-method.php', array( 'gateway' => $gateway ) );
					}
				} else {
					echo '<li>' . apply_filters( 'woocommerce_no_available_payment_methods_message', WC()->customer->get_country() ? __( 'Sorry, it seems that there are no available payment methods for your state. Please contact us if you require assistance or wish to make alternate arrangements.', 'woocommerce' ) : __( 'Please fill in your details above to see available payment methods.', 'woocommerce' ) ) . '</li>';
				}
			?>
		</ul>
	<?php endif; ?>
	<div class="form-row place-order">
		<noscript>
			<?php _e( 'Since your browser does not support JavaScript, or it is disabled, please ensure you click the <em>Update Totals</em> button before placing your order. You may be charged more than the amount stated above if you fail to do so.', 'woocommerce' ); ?>
			<br/><input type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="<?php esc_attr_e( 'Update totals', 'woocommerce' ); ?>" />
		</noscript>

		<?php wc_get_template( 'checkout/terms.php' ); ?>

		<?php do_action( 'woocommerce_review_order_before_submit' ); ?>

		<?php echo apply_filters( 'woocommerce_order_button_html', '<input type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr( $order_button_text ) . '" data-value="' . esc_attr( $order_button_text ) . '" />' ); ?>

		<?php do_action( 'woocommerce_review_order_after_submit' ); ?>

		<?php wp_nonce_field( 'woocommerce-process_checkout' ); ?>
        

        
	</div>
    
            		</div>
        
       <!--     <div class="need-help-box">
<hr />
<h4>Need Help?</h4>
<p>Contact our customer service team on<br/>
<strong style="font-size: 24px;">0800 411 <span>88 99</span></strong><br/>
(Calls may be recorded and monitored for quality and training purposes.)</p>

</div>
-->
        
                           
<!-- HEED HELP? -->
<div class="entry-content">
<div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12 vc_hidden-xs"><div class="vc_column-inner "><div class="wpb_wrapper">
<div class="wpb_raw_code wpb_content_element wpb_raw_html">
<div class="wpb_wrapper">
<div class="need-help-box">
<hr>
<h4>Need Help?</h4>
<p>Contact our customer service team on<br>
<span style="font-size: 24px;">0800 411 88 99</span><br>
(Calls may be recorded and monitored for quality and training purposes.)</p>
</div>
</div>
</div></div></div></div></div>


<div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12 vc_hidden-lg vc_hidden-md vc_hidden-sm"><div class="vc_column-inner "><div class="wpb_wrapper">
<div class="wpb_raw_code wpb_content_element wpb_raw_html">
<div class="wpb_wrapper">
<div class="need-help-box">
<hr>
<h4>Need Help?</h4>
<p>Contact our customer service team on<br>
<a class="tel" tabindex="-1" href="tel:0800 411 88 99" style="color:#43454b;">
<span style="font-size: 24px;">0800 411 88 99</span></a><br>
(Calls may be recorded and monitored for quality and training purposes.)</p>
</div>
</div>
</div></div></div></div></div>
</div>
<!-- END OF HEED HELP? -->
        
	</div>
</div></div></div><div class="col-b wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
            
                        <!-- Order Summary -->
            
<div class="borderbox">           
<div class="redwidget-brand" style="margin-top:-20px;">  <h2 style="text-align: center; color: #ffffff !important;">Order Summary</h2></div>
<br/>     
<center><h3>Credit/Debit card</h3></center>
<center><span class="adtotalamount" ><?php echo do_shortcode('[cart_total]');?></span></center>
<p style="text-align: center;">One off payment</p>   
<br/>   
<p style="text-align: center; margin-top:-10px; margin-bottom:10px;">
<img class="aligncenter size-full wp-image-3286" src="http://www.apswebquote.com/images/payment-cards.png" alt="payment-cards"/>
</p>        
</div>
 
<div class="borderboxtandc"> 

<?php
	// $termsURL = esc_url( wc_get_page_permalink( 'terms' ) );
	$termsURL = 'http://news-junction.com/termsandconditions/';
?>

<p style="text-align: center; padding:13px;   line-height: 16px;">
    <input type="checkbox" class="input-checkbox" name="terms" <?php checked( apply_filters( 'woocommerce_terms_is_checked_default', isset( $_POST['terms'] ) ), true ); ?> id="terms" />
    <label for="terms" class="checkbox"><?php printf( __( 'I&rsquo;ve read and accept the <br/><a href="%s" target="_blank">Terms &amp; Conditions</a>', 'woocommerce' ), $termsURL ); ?> <span class="required">*</span></label>
    <input type="hidden" name="terms-field" value="1" />
</p>



</div>
<!-- End of Order Summary -->

<div class="jcmc-buttons" style="display: inline!important;">
<button class="jcmc-button jcmc-nextprev jcmc-order alt" type="button" style="display: block;">Place order</button>
<a href="#" class="jcmc-button jcmc-nextprev jcmc-prev" style="display: inline-block; background: #CCCCCC!important;
    color: #666666;">Previous</a> 
</div>

      
            
            <!-- CONTENT
            <div class="h_iframe">
        <img class="ratio" src="http://www.news-junction.com/images/space.png">
        <iframe src="http://www.news-junction.com/WebQuoteApp/widget-personaldetails/" frameborder="0" allowfullscreen></iframe>
    </div> -->
            
  <!-- END CONTENT -->

		</div>
	</div>
</div></div></div></div>
					</div><!-- .entry-content -->

            
    
</div><!-- END -->
<?php
if ( ! is_ajax() ) {
	do_action( 'woocommerce_review_order_after_payment' );
}
