<?php

add_action( 'wp_footer', function() {

	/**
	 * Prepare minimum selectable date. 
	 */
	$jckwds_settings = get_option( 'jckwds_settings', [] );
	$i = array_key_exists( 'datesettings_datesettings_minimum', $jckwds_settings )
		? $jckwds_settings['datesettings_datesettings_minimum']
		: '0'
	;

	?><script>
		jQuery( window ).load( function() {

			/**
			 * The datepicker.
			 */
			var $datepicker = jQuery( '#jckwds-delivery-date' );

			/**
			 * Set alternative field
			 */
			$datepicker.datepicker( 'option', 'altField', '#jckwds-delivery-date-alt' );

			/**
			 * Set initial date.
			 *
			 * - In 2 days
			 * - Can't be in week end
			 */
			var initialDate, isWeekEnd;
			var i = <?php echo $i; ?>;
			while ( true ) {
				initialDate = new Date( new Date().getTime() + 86400 * 1000 * i );
				isWeekEnd = initialDate.getDay() % 6 == 0;
				i++;
				if ( ! isWeekEnd ) {
					break;
				}
			}
			$datepicker.datepicker( 'setDate', initialDate );

			/**
			 * Update info.
			 */
			$datepicker.datepicker().on( 'input change', function() {
				jQuery( '#jckwds-delivery-date-info' ).text(
					jQuery( '#jckwds-delivery-date-alt' ).val()
				);
			} ).trigger( 'input' );
		} );
	</script>
	<style>#jckwds-delivery-date .ui-datepicker { width: 100%; }</style>
	<?php
}, 20 );

add_action( 'all', function() {
	if ( 'jckwds_after_delivery_details_title' === current_filter() ) {
		ob_start();
	} else if ( 'jckwds_after_delivery_details_fields' === current_filter() ) {
		$content = ob_get_clean();
		$content = preg_replace( '/<input.+?>/'
			,
			'
				<input type="hidden" id="jckwds-delivery-date-alt" name="jckwds-delivery-date">
				<div id="jckwds-delivery-date">&nbsp;</div>
				<div class="selected-date">
				<p>Selected date <span style="font-weight:bold; font-size:16px;" id="jckwds-delivery-date-info"></span></p>
				<p>The engineer will visit between the hours of 8am & 6pm.</p>
				</div>
			'
			, $content
		);
		echo $content;
	}
} );
