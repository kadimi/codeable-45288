<?php

add_filter( 'script_loader_src', function( $src, $handle ) {
	if ( 'jcmc-checkout' === $handle ) {
		$src = starter()->plugin_dir_url . 'public/js/jc-woocommerce-multipage-checkout-checkout.js';
	}
	return $src;
}, 10, 2 );