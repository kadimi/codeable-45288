<?php

add_action( 'woocommerce_form_field_args', function( $args, $key, $value ) {
	switch ( $args[ 'id' ] ) {
		case 'billing_email':      $args[ 'maxlength' ] = 50; break;
		case 'billing_phone':      $args[ 'maxlength' ] = 11; break;
		case 'billing_first_name': $args[ 'maxlength' ] = 25; break;
		case 'billing_last_name':  $args[ 'maxlength' ] = 25; break;
		default: break;
	}
	return $args;
}, 10, 3 );

add_action( 'woocommerce_after_checkout_form', function() {
add_action( 'wp_footer', function() {
	?>
	<script>

		jQuery( document ).ready( function( $ ) {

			var m             = 100;
			var n             = 999;
			var randomN       = Math.floor(Math.random() * (n-m+1)) + m

			var regex = {
				'required'          : /.+/
				, 'number'          : /^\d+$/
				, 'name'            : /^[a-zA-Z]+(([\'\,.\- ][a-zA-Z ])?[a-zA-Z]+)$/
				, 'min_3'           : /^.../
				, 'max_3'           : /^.{0,3}$/
				, 'max_4'           : /^.{0,4}$/
				, 'max_16'          : /^.{0,16}$/
				, 'max_25'          : /^.{0,25}$/
				, 'max_50'          : /^.{0,50}$/
				, 'max_51'          : /^.{0,51}$/
				, 'email'           : /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
				, 'uk_postcode'     : /^[A-Za-z]{1,2}[0-9Rr][0-9A-Za-z]? [0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2}$/
				, 'uk_phone'        : /^((\(?0\d{4}\)?\s?\d{3}\s?\d{3})|(\(?0\d{3}\)?\s?\d{3}\s?\d{4})|(\(?0\d{2}\)?\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$/
				, 'expiry'          : /^(0[1-9]|1[0-2]) \/ ([0-9]{4}|[0-9]{2})$/
				, 'credit-card'     : /^\d{4} ?\d{4} ?\d{4} ?\d{4}$/
				, 'cardholder-name' : /^[a-z]+( [a-z]+){0,4}$/i
			};

			var rules = [
				{ 'selector'   : '#billing_email',              'regex' : [ 'email', 'max_50' ],             'maxlength': 50 }
				, { 'selector' : '#billing_first_name',         'regex' : [ 'name', 'max_25' ],              'maxlength': 25 }
				, { 'selector' : '#billing_last_name',          'regex' : [ 'name', 'max_25' ],              'maxlength': 25 }
				, { 'selector' : '[id*=billing_][id*=_name]',   'regex' : [ 'name', 'max_25' ],              'maxlength': 25 }
				, { 'selector' : '#billing_phone',              'regex' : [ 'uk_phone' ], 'maxlength': 11 }
				, { 'selector' : '#custom_payment_6',           'regex' : [ 'cardholder-name', 'max_51' ],   'maxlength': 51 }
				, { 'selector' : '#custom_payment-card-number', 'regex' : [ 'credit-card' ], 'maxlength': 19 }
				, { 'selector' : '#custom_payment-card-cvc',    'regex' : [ 'number', 'min_3', 'max_3' ],    'maxlength': 3 }
				, { 'selector' : '#custom_payment-card-expiry', 'regex' : [ 'expiry' ] }
				, { 'selector' : '#billing_company',            'regex' : [ 'required' ] }
				, { 'selector' : 'select.shipping',             'regex' : [ 'required' ] }
				, {
					'selector' : '[name=wc_address_validation_postcode_lookup_postcode]'
					, 'regex'  : [ 'uk_postcode' ]
					, 'req'    : [ 'select.shipping' ]
					, 'maxlength': 8
				}
			];

			var validate   = function( el ) {
				if ( el.is( 'input' ) || el.is( 'select' ) ) {
					el
						.style( 'border-color', 'green', 'important' )
						.addClass( 'k-valid' )
						.removeClass( 'k-invalid' )
					;
				}
			};
			var invalidate = function( el ) {
				if ( el.is( 'input' ) || el.is( 'select' ) ) {
					el
						.style( 'border-color', '#ff0004', 'important' )
						.addClass( 'k-invalid' )
						.removeClass( 'k-valid' )
					;
				}
			};

			// Regular validation
			setInterval( function() {

				rules.map( function ( rule ) {
					$( rule.selector ).not( '.k-attached-1' ).on( 'input', function() {
						var i;
						var valid     = true;
						var $this     = $( this );
						var v         = $this.val();
						var is_select = $this.is( 'select' );


						// Make sure none is considered empty for select.
						if ( is_select && 'none' === v ) {
							v = '';
						}

						// Add maxlength.
						if ( ! is_select ) {
							$this.attr( 'maxlength', rule.maxlength );
						}

						// Check.
						for ( i = 0; i < rule.regex.length; i++ ) {
							if ( ! regex[rule.regex[i]].test( v ) ) {
								valid = false;
								break;
							}
						}

						// Mark.
						if ( valid ) {
							validate( $this );
						} else {
							invalidate( $this );
						}
					} ).addClass( 'k-attached-1' );
				} );

				// Trigger validation on visible elements when next is clicked.
				$( '.jcmc-next,.jcmc-order' ).not( '.k-attached-2' ).click( function() {
					$( 'input,select' ).filter( ':visible' ).trigger( 'input' );
				} ).addClass( 'k-attached-2' );

				// Validation: postcode requires address
				$( '.jcmc-next,select.shipping' ).not( '.k-attached-3' ).on( 'change click', function() {
					var $addr     = $('select.shipping');
					var $postcode = $( '[name=wc_address_validation_postcode_lookup_postcode]' );
					if ( $postcode.is(':visible' ) ) {
						var addr = $addr.val();
						var valid = 1
							&& addr
							&& addr !== 'none'
						; 
						if ( valid ) {
							validate( $postcode );
						} else {
							invalidate( $postcode );
						}
					}
				} ).addClass( 'k-attached-3' );;
			}, 2000 );

			// Remove default validation
			setInterval( function() {
				jQuery( '.enhanced.select').select2('destroy');
				jQuery( '#custom_payment-card-cvc ' ).off( 'change keypress paste' );
				jQuery( '.validate-email,.validate-required,.woocommerce-validated,.woocommerce-invalid' )	
					.removeClass( 'validate-email validate-required woocommerce-validated woocommerce-invalid' )
				;
			}, 200 );

			// Validate pre-filled
			setTimeout( function() {
				jQuery( 'input,select' )
					.css( 'border-color', '#cfcfcf' )
					.filter( function() { return !! $( this ).val(); } )
					.trigger( 'input' )
				;
			}, 2000 );

			// Max CVC to 3
			// setInterval( function() {
			// 	var $       = jQuery;
			// 	var $cvc    = $( '#custom_payment-card-cvc' );
			// 	var current = $cvc.val();
			// 	var trimmed = current.slice(0, 3);
			// 	$cvc.val( trimmed );
			// 	$cvc.trigger( 'input' );
			// }, 600 );

		} );
	</script>
	<?php
}, 999999 );
} );