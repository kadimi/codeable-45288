<?php

/**
 * Force order
 */
add_action( 'option_jcmc_checkout_steps', function() {
	return serialize( [
		[
			'name' => 'Delivery',
			'selector' => '{ORDER_DETAILS}',
			'in_form' => 'yes',
		],
		[
			'name' => 'Personal Details',
			'selector' => '{BILLING_FIELDS}, {SHIPPING_FIELDS}',
			'in_form' => 'yes',
		],
		[
			'name' => 'Payment',
			'selector' => '{PAYMENT_DETAILS}',
			'in_form' => 'yes',
		],
	] );
} );

/**
 * Output steps bar HTML.
 * @param  Integer $step_number The step number.
 */
function aps_steps( $step_number ) {

	$steps = [
		'Appointment Date',
		'Personal Details',
		'Payment Details',
		'Confirmation',
	];

	?>	
	<div id="aps-steps" class=" aps-steps-tabs-top">
		<ul class="aps-steps-tabs  aps-steps-arrows aps-steps-blocks aps-steps-wide" id="aps-steps-tabs">
		<?php
		foreach ( $steps as $i => $step ) {
			printf( '
				<li class="aps-steps-%s %s">
					<a id="aps-steps-trigger-%d" href="#">
						<span class="aps-steps-tab-span">
							<span class="aps-steps-number">%d.</span>
							%s
						</span>
					</a>
				</li>'
				, ( $i % 2) ? 'odd' : 'even'
				, ( $i + 1 === $step_number ) ? 'aps-steps-enabled aps-steps-active-link' : ''
				, $i
				, $i + 1
				, $step
			);
		}
		?>
		</ul>
	</div>
	<?php 
}
