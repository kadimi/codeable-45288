<?php

add_action( 'all', function() {

	if ( 'woocommerce_before_checkout_billing_form' === current_filter() ) {
		ob_start();
	}

	if ( 'woocommerce_after_checkout_billing_form' === current_filter() ) {
		$html = ob_get_clean();
		$html = str_replace( 'value="Please select"', 'value=""', $html );
		echo $html;
	}
} );
