<?php

add_action( 'wp_head', function() {
	?><style>
		.codeable-45288-hidden {
			display: none;
		}
	</style><?php
} );

add_action( 'body_class', function() {
	if ( is_checkout() ) {
		$classes[] = 'codeable-45288-hidden';
	}
	return $classes;
} );

add_action( 'wp_footer', function() {
	?><script>
		jQuery( document ).ready( function( $ ) {

			// Show page
			setTimeout( function() {
				var $ = jQuery;
				$( 'body' ).show();
			}, 1000 )

			// Hide CC form first time
			var $container = $( '.wc_payment_methods' ).parent().parent();
			$container.addClass( 'codeable-45288-hidden' ).fadeTo( 500, 0 );
			// Keep hiding it
			setInterval( function() {
				var $ = jQuery;
				var $container = $( '.wc_payment_methods' ).parent().parent();
				if ( ! $container.is( ':visible' ) ) {
					$container.addClass( 'codeable-45288-hidden' ).fadeTo( 500, 0 );
				}
			}, 500 );
			// Show it when on its page
			$( document.body ).on( 'updated_checkout', function() {
				var $container = $( '.wc_payment_methods' ).parent().parent();
				$container
					.filter( ':visible' )
					.removeClass( 'codeable-45288-hidden' )
					.fadeTo( 500, 1 )
				;
			} );
		} );
	</script><?php
} );
