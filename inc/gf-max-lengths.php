<?php

add_filter( 'gform_field_content', function( $field_content, $field, $value, $lead_id, $form_id ) {

	if ( 1 === $form_id ) {
		switch ( $field->id ) {
			case 16: return str_replace( '<input', '<input maxlength="25"', $field_content ); // First name
			case 17: return str_replace( '<input', '<input maxlength="25"', $field_content ); // Last name
			case 15: return str_replace( '<input', '<input maxlength="50"', $field_content ); // Email
			case 14: return str_replace( '<input', '<input maxlength="11"', $field_content ); // Phone
			case 10: return str_replace( '<input', '<input maxlength="8"', $field_content ); // Postcode
		}
	}

	return $field_content;
}, 10, 5 );
