<?php

add_filter( 'script_loader_src', function( $src, $handle ) {
	if ( 'wpr-form-validation' === $handle ) {
		$src = starter()->plugin_dir_url . 'public/js/wpr-form-fields-validate.js';
	}
	return $src;
}, 10, 2 );
