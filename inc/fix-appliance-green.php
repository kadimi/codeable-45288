<?php

add_action( 'wp_footer', function() {
	?>
	<script>

		jQuery( document ).ready( function( $ ) {
			setInterval( function() {
				var $    = jQuery;
				$radios  = $( '[class*=gchoice_1_12_] :radio' );
				
				$radios.each( function() {
					var $this   = $( this );
					var $label  = $this.siblings( 'label' ).first();
					var checked = $this.is( ':checked' );

					$label.css( 'border-color', checked
						? 'green'
						: 'transparent'
					);

				} );
				$checked = $radios.filter( ':checked' );
			}, 100 );
		} );
	</script>
	<?php
} );
