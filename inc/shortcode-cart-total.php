<?php

/**
 * Create and register shortcode `[cart_total]`.
 */
add_shortcode( 'cart_total', function() {
	return strip_tags( WC()->cart->get_cart_total() );
} );

/**
 * Make sure shortcode is parsed.
 */
add_action( 'all', function() {
	if ( 'storefront_page_before' === current_filter() ) {
		ob_start();
	}
	if ( 'storefront_page_after' === current_filter() ) {
		$content = ob_get_clean();
		$content = str_replace( '[cart_total]', do_shortcode( '[cart_total]' ), $content );
		echo $content;
	}
} );

/**
 * Bonus! Replace `£#.##` with cart `[cart_total]`
 */
add_action( 'all', function() {
	if ( 'storefront_page_before' === current_filter() ) {
		ob_start();
	}
	if ( 'storefront_page_after' === current_filter() ) {
		$content = ob_get_clean();
		$content = str_replace( '£#.##', do_shortcode( '[cart_total]' ), $content );
		echo $content;
	}
} );
