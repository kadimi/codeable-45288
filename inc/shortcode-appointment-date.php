<?php

/**
 * Create and register shortcode `[cart_total]`.
 */
add_shortcode( 'appointment_date', function() {

	$appointment_date   = '01/01/1970';
	$format_on_db       = 'd/m/Y';
	$format_for_display = 'l, F, jS Y';

	$customerOrder = get_posts( apply_filters( 'woocommerce_my_account_my_orders_query', array(
		'numberposts' => 1,
		'meta_key'    => '_customer_user',
		'meta_value'  => get_current_user_id(),
		'post_type'   => 'shop_order',
		'post_status' => array_keys( wc_get_order_statuses() )
	) ) );

	if($customerOrder) { 	
		$order = new WC_Order();
		$order->populate( $customerOrder[0] );
		$appointment_date = get_post_meta( $order->id, 'jckwds_date', true );

		$date = DateTime::createFromFormat( $format_on_db, $appointment_date );
		$formatted_date = $date->format( $format_for_display );
	}

	return sprintf(
		'
		<div class="wpb_wrapper" style="height: 100px;">
			<p style="text-align: center; font-size: 14px; margin: 0">%s:</p>
			<p style="text-align: center; font-size: 18px; margin: 0">%s</p>
		</div>
		'
		, 'APPOINTMENT DATE'
		, $formatted_date
	);
} );
