<?php

add_filter( 'script_loader_src', function( $src, $handle ) {
	if ( 'real-time-validation-live-validation' === $handle ) {
		$src = starter()->plugin_dir_url . 'public/js/livevalidation_standalone.js';
	}
	return $src;
}, 10, 2 );

add_action( 'xp_footer', function() {
	?><script>
		jQuery( document ).ready( function( $ ) {
			$( document ).on( 'keyup', '.ginput_container input',  debounce( function() {
				jQuery( this ).blur().focus();
			 }, 600 ) );

			function debounce(func, wait) {
				var timeout;
				return function() {
					var context = this, args = arguments;
					var later = function() {
						timeout = null;
						func.apply(context, args);
					};
					clearTimeout(timeout);
					timeout = setTimeout(later, wait);
				};
			};
		} );
	</script>
	<?php
} );
