<?php

/**
 * Remove terms added by wprautocomplete plugin.
 */
add_action( 'init', function() {
	remove_action( 'woocommerce_after_order_notes', 'wpr_move_terms_and_conditions', 90 );
} );
