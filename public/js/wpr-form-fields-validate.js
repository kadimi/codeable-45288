(function ($) {
    if ($.fn.style) {
        return;
    }

    // Escape regex chars with \
    var escape = function (text) {
        return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
    };

    // For those who need them (< IE 9), add support for CSS functions
    var isStyleFuncSupported = !!CSSStyleDeclaration.prototype.getPropertyValue;
    if (!isStyleFuncSupported) {
        CSSStyleDeclaration.prototype.getPropertyValue = function (a) {
            return this.getAttribute(a);
        };
        CSSStyleDeclaration.prototype.setProperty = function (styleName, value, priority) {
            this.setAttribute(styleName, value);
            var priority = typeof priority != 'undefined' ? priority : '';
            if (priority != '') {
                // Add priority manually
                var rule = new RegExp(escape(styleName) + '\\s*:\\s*' + escape(value) +
                    '(\\s*;)?', 'gmi');
                this.cssText =
                    this.cssText.replace(rule, styleName + ': ' + value + ' !' + priority + ';');
            }
        };
        CSSStyleDeclaration.prototype.removeProperty = function (a) {
            return this.removeAttribute(a);
        };
        CSSStyleDeclaration.prototype.getPropertyPriority = function (styleName) {
            var rule = new RegExp(escape(styleName) + '\\s*:\\s*[^\\s]*\\s*!important(\\s*;)?',
                'gmi');
            return rule.test(this.cssText) ? 'important' : '';
        }
    }

    // The style function
    $.fn.style = function (styleName, value, priority) {
        // DOM node
        var node = this.get(0);
        // Ensure we have a DOM node
        if (typeof node == 'undefined') {
            return this;
        }
        // CSSStyleDeclaration
        var style = this.get(0).style;
        // Getter/Setter
        if (typeof styleName != 'undefined') {
            if (typeof value != 'undefined') {
                // Set style property
                priority = typeof priority != 'undefined' ? priority : '';
                style.setProperty(styleName, value, priority);
                return this;
            } else {
                // Get style property
                return style.getPropertyValue(styleName);
            }
        } else {
            // Get CSSStyleDeclaration
            return style;
        }
    };
})(jQuery);

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

(function ($) {

    return;

    $('.site-content').on('click tap', '.jcmc-next', function () {

        var current_form = $(this).closest('#jcmc-wrap').find('form.woocommerce-checkout');
        var input_text = current_form.find('input[type=text]');
        var input_select = current_form.find('select');

        $(input_text).each(function () {

            if ($(this).attr('type') != 'hidden') {
                if ('billing_email' === $(this).attr('name')) {
                    if (validateEmail($(this).val().trim())) {
                        $(this).style('border-color', '#388e3c', 'important');
                    } else {
                        $(this).style('border-color', 'red', 'important');
                    }
                } else if ('shipping_address_2' === $(this).attr('name')) {
                    // nothing
                } else if ('shipping_state' === $(this).attr('name')) {
                    $(this).style('border-color', '#388e3c', 'important');
                } else if ($(this).val().trim() === '') {
                    $(this).style('border-color', 'red', 'important');

                } else {
                    $(this).style('border-color', '#388e3c', 'important');

                }
            }
        });

        $(input_select).each(function () {
            var name = $(this).attr('name');
            if ('wc_address_validation_postcode_lookup_postcode_results' !== name) {
                var select2_id = '#s2id_' + $(this).attr('id');
                var s2_html_element = $(select2_id).find('.select2-choice');
                if ($(this).val().trim() === '') {
                    s2_html_element.style('border-color', 'red', 'important');
                }
                else {
                    s2_html_element.style('border-color', '#388e3c', 'important');
                }
            }
        });
    });

    $(document).on("select2-selecting", ".wc-address-validation-enhanced-select", function () {
        setTimeout(function () {
            var parent_element = $('.woocommerce-shipping-fields');
            var input_text = parent_element.find('input[type=text]');
            var input_select = parent_element.find('select');

            $(input_text).each(function () {

                if ($(this).attr('type') != 'hidden') {
                    if ('billing_email' === $(this).attr('name')) {
                        if (validateEmail($(this).val())) {
                            $(this).style('border-color', '#388e3c', 'important');
                        } else {
                            $(this).style('border-color', 'red', 'important');
                        }
                    } else if ('shipping_address_2' === $(this).attr('name')) {
                        //$(this).style('border-color', '#388e3c', 'important');
                    } else if ($(this).val().trim() === '') {
                        $(this).style('border-color', 'red', 'important');

                    } else {
                        $(this).style('border-color', '#388e3c', 'important');

                    }
                }
            });

            $(input_select).each(function () {
                var name = $(this).attr('name');

                if ('wc_address_validation_postcode_lookup_postcode_results' !== name) {
                    var select2_id = '#s2id_' + $(this).attr('id');
                    var s2_html_element = $(select2_id).find('.select2-choice');
                    if ($(this).val().trim() === '') {
                        s2_html_element.style('border-color', 'red', 'important');
                    }
                    else {
                        s2_html_element.style('border-color', '#388e3c', 'important');
                    }
                } else if ('wc_address_validation_postcode_lookup_postcode_results' === name) {
                    var elem_select = $('div.wc-address-validation-enhanced-select').find('a.select2-choice');
                    elem_select.style('border-color', '#388e3c', 'important');
                }
            });
        }, 300);

    });

    // card holder validate
    // $(document).on('focusout', '#custom_payment_3', function () {
    //     var value = $(this).val().trim();
    //     if (value.length <= 3) {
    //         $(this).closest('p.form-row').removeClass('woocommerce-validated').addClass('woocommerce-invalid');
    //     }
    // });

    // $(document).on('focusin', '#custom_payment_3', function () {
    //     $(this).closest('p.form-row').removeClass('woocommerce-invalid').addClass('woocommerce-validated');
    // });

    // card number validate
    // $(document).on('focusout', '#custom_payment-card-number', function () {
    //     var value = $(this).val().trim();
    //     var formated_card_no = $.payment.formatCardNumber(value);
    //     var validate_this = $.payment.validateCardNumber(formated_card_no);
    //     if (!validate_this) {
    //         $(this).closest('p.form-row').removeClass('woocommerce-validated').addClass('woocommerce-invalid');
    //     }
    // });

    // $(document).on('focusin', '#custom_payment-card-number', function () {
    //     $(this).closest('p.form-row').removeClass('woocommerce-invalid').addClass('woocommerce-validated');
    // });

    // validate expiry
    // $(document).on('focusout', '#custom_payment-card-expiry', function () {
    //     var value = $(this).payment('cardExpiryVal');
    //     var validate_this = $.payment.validateCardExpiry(value);
    //     if (!validate_this) {
    //         $(this).closest('p.form-row').removeClass('woocommerce-validated').addClass('woocommerce-invalid');
    //     }
    // });

    // $(document).on('focusin', '#custom_payment-card-expiry', function () {
    //     $(this).closest('p.form-row').removeClass('woocommerce-invalid').addClass('woocommerce-validated');
    // });

    //validate card code CVC
    // $(document).on('focusout', '#custom_payment-card-cvc', function () {
    //     var value = $(this).val().trim();
    //     var cardType = $.payment.cardType($('#custom_payment-card-number').val());

    //     var validate_this = $.payment.validateCardCVC(value, cardType);
    //     if (!validate_this) {
    //         $(this).closest('p.form-row').removeClass('woocommerce-validated').addClass('woocommerce-invalid');
    //     }
    // });

    // $(document).on('focusin', '#custom_payment-card-cvc', function () {
    //     $(this).closest('p.form-row').removeClass('woocommerce-invalid').addClass('woocommerce-validated');
    // });
}(jQuery));
